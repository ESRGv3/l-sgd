# Train me if you can: decentralizedlearning on the deep edge

This repository contains the code and experiments for the paper:

> Train me if you can: decentralized learning on the deep edge

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#L-SGD">L-SGD</a></li>
    <li><a href="#SGD (float-32) vs. L-SGD (float-32)">SGD (float-32) vs. L-SGD (float-32)</a></li>
    <li><a href="#L-SGD (float-32)  vs. L-SGD (int-8)">L-SGD (float-32)  vs. L-SGD (int-8)</a></li>
  </ol>
</details>


# L-SGD
The end of Moore's Law aligned with rising concerns about data privacy is forcing machine learning (ML) to shift from the cloud to the deep edge, near to the data source. In the next-generation ML systems, the inference and part of the training process will be performed right on the edge, while the cloud will be responsible for major ML model updates. This new computing paradigm, referred to by academia and industry researchers as federated learning, alleviates the cloud and network infrastructure while increasing data privacy. Recent advances have made it possible to efficiently execute the inference pass of quantized artificial neural networks (ANNs) on Arm Cortex-M and RISC-V (RV32IMCXpulp) microcontroller units (MCUs), unlocking an entirely new class of smart applications. Nevertheless, the training remains confined to the cloud, imposing the transaction of high volumes of private data over a network and leading to unpredictable delays when ML applications try to adapt to adversarial environments. To tackle this issue, we make a first attempt to run a decentralized training in Arm Cortex-M MCUs. We propose L-SGD, a lightweight implementation of the stochastic gradient descent (SGD) optimized for maximum speed and minimal memory footprint on Arm Cortex-M MCUs. We also evaluate the feasibility of quantized training in a federated learning scenario. For a fully-connected ANN trained on the MNIST dataset, L-SGD is 4.20x faster than the SGD reference while requiring only 2.80% of the memory.

## SGD (float-32) vs. L-SGD (float-32)

<table class="tg">
<thead>
  <tr>
    <th class="tg-c3ow"></th>
    <th class="tg-c3ow" colspan="2">MNIST</th>
    <th class="tg-c3ow" colspan="2">CogDist</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-c3ow"></td>
    <td class="tg-c3ow">SGD</td>
    <td class="tg-c3ow">L-SGD</td>
    <td class="tg-c3ow">SGD</td>
    <td class="tg-c3ow">L-SGD</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Accuracy (%)</td>
    <td class="tg-c3ow">92.45</td>
    <td class="tg-c3ow">93.54</td>
    <td class="tg-c3ow">83.51</td>
    <td class="tg-c3ow">82.18</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Memory footprint (KBytes)</td>
    <td class="tg-c3ow">135.64</td>
    <td class="tg-c3ow">3.79</td>
    <td class="tg-c3ow">6.82</td>
    <td class="tg-c3ow">0.64</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Latency (ms/sample)</td>
    <td class="tg-c3ow">75.09</td>
    <td class="tg-c3ow">17.84</td>
    <td class="tg-c3ow">8.90</td>
    <td class="tg-c3ow">8.51</td>
  </tr>
</tbody>
</table>

## L-SGD (float-32) vs. L-SGD (int-8)

<table class="tg">
<thead>
  <tr>
    <th class="tg-c3ow"></th>
    <th class="tg-c3ow" colspan="2">MNIST</th>
    <th class="tg-c3ow" colspan="2">CogDist</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-c3ow"></td>
    <td class="tg-c3ow">L-SGD<br>(float-32)</td>
    <td class="tg-c3ow">L-SGD<br>(float-8)</td>
    <td class="tg-c3ow">L-SGD<br>(float-32)</td>
    <td class="tg-c3ow">L-SGD<br>(float-8)</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Accuracy (%)</td>
    <td class="tg-c3ow">92.54</td>
    <td class="tg-c3ow">92.83</td>
    <td class="tg-c3ow">91.95</td>
    <td class="tg-c3ow">92.79</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Memory footprint (KBytes)</td>
    <td class="tg-c3ow">3.79</td>
    <td class="tg-c3ow">1.03</td>
    <td class="tg-c3ow">0.64</td>
    <td class="tg-c3ow">0.25</td>
  </tr>
  <tr>
    <td class="tg-c3ow">Latency (ms/sample)</td>
    <td class="tg-c3ow">17.84</td>
    <td class="tg-c3ow">7.17</td>
    <td class="tg-c3ow">8.51</td>
    <td class="tg-c3ow">4.49</td>
  </tr>
</tbody>
</table>

