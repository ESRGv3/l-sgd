#include "forward_pass.h"

// Feed inputs to input layer
void feed_input(layer *in_layer, float sample[][INPUT_FEATURES], int input_size, int sample_idx)
{
    //Assign an input feature to each input neuronb
    for(int neuron_idx=0; neuron_idx<input_size; neuron_idx++)
    {
        float x = sample[sample_idx][neuron_idx];
        in_layer[0].neu[neuron_idx].actv = sample[sample_idx][neuron_idx];
        //printf("Input: %f\n", in_layer[0].neu[neuron_idx].actv);
    }
}


// Forward pass 
void forward_prop(layer* lay, int* layers_dims,  int* activation_functions)
{
    int i, j, k;

    for (i = 1; i < N_LAYERS; i++)
    {
        for (j = 0; j < layers_dims[i]; j++)
        {
            lay[i].neu[j].z = lay[i].neu[j].bias;

            for (k = 0; k < layers_dims[i - 1]; k++)
            {
                lay[i].neu[j].z = lay[i].neu[j].z + ((lay[i - 1].neu[k].out_weights[j]) * (lay[i - 1].neu[k].actv));
            }

            if (i == N_LAYERS - 1)
                i = N_LAYERS - 1;
            lay[i].neu[j].actv = node_activation_function(lay[i].neu[j].z, activation_functions[i-1]);

            //// Relu Activation Function for Hidden Layers
            //if (i < N_LAYERS - 1)
            //{
            //    if ((lay[i].neu[j].z) < 0)
            //    {
            //        lay[i].neu[j].actv = 0;
            //    }
//          //
            //    else
            //    {
            //        lay[i].neu[j].actv = lay[i].neu[j].z;
            //    }
            //}
            //
            //// Sigmoid Activation function for Output Layer
            //else
            //{
            //    lay[i].neu[j].actv = 1 / (1 + exp(-lay[i].neu[j].z));
            //    //printf("Output: %d\n", (int)round(lay[i].neu[j].actv));
            //    //printf("\n");
            //}
        }
    }
}

float node_activation_function(const float neuron_input, int act_func)
{
	float activation_value = 0;

	switch (act_func)
	{
	case RELU:
		if (neuron_input <= 0)
			activation_value = 0;
		else
			activation_value = neuron_input;
		break;

	case SIGMOID:
		//if (neuron_input >= 10)
		//	activation_value=1.0;
		//else if (neuron_input <= -10)
		//	activation_value = 0.0;
		//else
		//	activation_value = 1.0 / (1.0 + exp(-neuron_input));
        activation_value = 1.0 / (1.0 + exp(-neuron_input));
		break;

	case TANH:
		activation_value = (exp(neuron_input) - exp(-neuron_input)) / (exp(neuron_input) + exp(-neuron_input));
		break;

	default:
		break;
	}
	return activation_value;
}

float test_nn(layer* lay, int* layers_dims, int* activation_functions, float **samples, float *labels, int n_samples)
{
    int node_idx;
    float max_value;
    float accuracy=0;
    int correct_predictions=0;

    for (int sample_idx = 0; sample_idx < n_samples; sample_idx++)
    {
        node_idx = 0;
        max_value = 0.0;

        //Forward pass
        feed_input(lay, samples, INPUT_FEATURES, sample_idx);
        forward_prop(lay, layers_dims, activation_functions);

        //Get most likely output
        if (layers_dims[N_LAYERS - 1] > 1)
        {
            for (int output_idx = 0; output_idx < layers_dims[N_LAYERS - 1]; output_idx++)
            {
                float output_value = lay[N_LAYERS - 1].neu[output_idx].actv;
                if (max_value < lay[N_LAYERS - 1].neu[output_idx].actv)
                {
                    max_value = lay[N_LAYERS - 1].neu[output_idx].actv;
                    node_idx = output_idx;
                }
            }
        }

        else if (lay[N_LAYERS - 1].neu[0].actv >= 0.5)
        {
            node_idx = 1;
        }

        else
            node_idx = 0;

        
        //Evaluate prediction (true/false)
        if (node_idx == labels[sample_idx])
            correct_predictions++;
    }

    //Calculate test accuracy
    accuracy = (float)correct_predictions/(float)n_samples;

    return accuracy;
}
