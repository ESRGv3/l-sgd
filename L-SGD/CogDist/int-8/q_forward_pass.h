#ifndef QFP_H
#define QFP_H

#include "nn_params.h"
#include "arm_math.h"

 typedef struct {
	q7_t layer_0_out[LAYER_0_DIM];
	q7_t layer_1_out[LAYER_1_DIM];
	q7_t layer_2_out[LAYER_2_DIM];
} q_forward_propagation_buffers;

void forward_pass_q7(q7_t *input_data, ann_model_q7 *model, q7_t *buffer1, q7_t *buffer2, q7_t *buffer3, q15_t *buffer_aux, uint8_t* out_formats);

#endif
