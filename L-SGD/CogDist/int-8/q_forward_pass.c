#include "q_forward_pass.h"
#include "arm_nnfunctions.h"

void forward_pass_q7(q7_t *input_data, ann_model_q7 *model, q7_t *buffer1, q7_t *buffer2, q7_t *buffer3, q15_t *buffer_aux, uint8_t* out_formats)
{
	uint8_t* out_qformat = out_formats;
	arm_fully_connected_q7(
		input_data, model->dense0_wt, model->model_dimensions[0], model->model_dimensions[1], model->dense0_bias_shift,
		model->dense0_out_shift, model->dense0_bias, buffer1, buffer_aux);
	arm_nn_activations_direct_q7(
		buffer1, model->model_dimensions[1], *out_qformat, ARM_TANH);
	
	out_qformat++;

	arm_fully_connected_q7(
		buffer1, model->dense1_wt, model->model_dimensions[1], model->model_dimensions[2], model->dense1_bias_shift,
		model->dense1_out_shift, model->dense1_bias, buffer2, buffer_aux);
	arm_nn_activations_direct_q7(
		buffer2, model->model_dimensions[2], *out_qformat, ARM_TANH);

	out_qformat++;
	
	arm_fully_connected_q7(
		buffer2, model->dense2_wt, model->model_dimensions[2], model->model_dimensions[3], model->dense2_bias_shift,
		model->dense2_out_shift, model->dense2_bias, buffer3, buffer_aux);
	arm_nn_activations_direct_q7(
		buffer3, model->model_dimensions[3], *out_qformat, ARM_SIGMOID);
}