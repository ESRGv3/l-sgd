#include "training_process.h"
#include "quantization.h"

/*
	@brief	neural network training process
	@param[in, out]		model					ML model
	@param[in]			train_set_size			number of train samples
	@param[in]			test_set_size			number of validation samples
	@param[in]			training_samples		training subset values
	@param[in]			training_labels			training subset labels
	@param[in]			validation_samples		validation subset values
	@param[in]			validation_labels		validation subset labels
	@param[in]			epochs					number of epochs
	@param[in]			learning_rate			training learning rate	
	
    @details
	
	Training process functions performs a traing over the input model
	with the defined hyperparameters (epochs and learning rate) and 
	with the dataset (subset of training and subset of validation).
	The training process only proceeds with unitary batch size due to
	memory constraints.
*/

void training_process(ann_model_q7* model_q7, int train_set_size, int test_set_size, float* training_samples, float* training_labels, float* validation_samples, float* validation_labels, unsigned char epochs, float learning_rate)
{
	float train_acc, val_acc;
	for (int curr_epoch = 1; curr_epoch <= epochs; curr_epoch++)             //run n epochs
	{
		//	Training
		float* sample = training_samples;
		float* label = training_labels;
		for (int sample_index = 0; sample_index < train_set_size; sample_index++)         //iterate input samples
		{
			//back_prop(sample, label, model, model_q7, learning_rate);
			q_back_prop(sample, label, model_q7, learning_rate);
			label++;
			sample += INPUT_DIM;
		}
	}
}


/*
	@brief	neural network validation process
	@param[in]			model					ML model
	@param[in]			values					validation subset values
	@param[in]			samples					validation subset labels
	@param[in]			data_size				number of validation samples
	@return		The function returns the validation accuracy
*/

float validation_process(ann_model* model, float* values, float* labels, int data_size, forward_propagation_buffers* fwd_p, train_log_data* train_log)
{
	int correct_predictions = 0;
	float* input_sample	= values;
	float* output_label	= labels;

	for (int sample_index = 0; sample_index < data_size; sample_index++)
	{
	forward_pass_val(input_sample, model, fwd_p, train_log);

		if (fwd_p->layer_2_out[0] >= 0.5 && *output_label == 1)
			correct_predictions++;

		else if (fwd_p->layer_2_out[0] < 0.5 && *output_label == 0)
			correct_predictions++;

		input_sample += INPUT_DIM;
		output_label++;
	}

	return (float)correct_predictions/data_size;
}

float validation_process_q7(ann_model_q7* model, float* values, float* labels, int data_size, q7_t *buffer1, q7_t *buffer2, q7_t *buffer3, q15_t *buffer_aux, uint8_t* out_formats)
{
	q7_t values_q7[6];
	int correct_predictions = 0;

	for (int sample_index = 0; sample_index < data_size; sample_index++)
	{
		quantize_q7(values, 6, 7, values_q7);
		forward_pass_q7(values_q7, model, buffer1, buffer2, buffer3, buffer_aux, out_formats);
		q7_t prediction = buffer3[0];

		if (prediction >= 64 && *labels == 1)
			correct_predictions++;

		else if (prediction < 64 && *labels == 0)
			correct_predictions++;

		values += INPUT_DIM;
		labels++;
	}

	return (float)correct_predictions/data_size;
}
