#ifndef QUANTIZATION_H
#define QUANTIZATION_H

#include "arm_math.h"
#include "nn_params.h"

void quantize_q7(float* input, int32_t data_size, int32_t frac_bits, q7_t* output);

void get_qformats(train_log_data* val_log, uint8_t* out_formats);

int get_qfrac_bits(float* buff, int buff_dim);

void quantize_model(ann_model *model, ann_model_q7 *model_q7, uint8_t* out_formats_iw);

void dequantize_model(ann_model *model, ann_model_q7 *model_q7);

void dequantize_q7(q7_t* input, int32_t data_size, int32_t frac_bits, float* output);

void update_shifts(uint8_t* out_fb, uint8_t* in_fb, ann_model_q7* model);

#endif