#include "q_backward_pass.h"

q15_t q_node_loss_pd(loss_functions loss, q15_t output_trg, q15_t model_out)
{
	int32_t loss_derivative = 0;
	q15_t max_value = (1 << Q15_FRAC_BITS);

	q15_t label = output_trg;
	q15_t output = model_out;

	switch (loss)
	{
	case MSE:
		loss_derivative = output - output_trg;
		break;

	case BCE:
		if(output == 0)
			output = 1;
		
		else if(output == max_value)
			output = max_value - 1;
		
		int32_t num = ((output - output_trg) << Q15_FRAC_BITS);
		int32_t den = (output*output);
		den = den >> 1;
		den = output - den;
		loss_derivative = num/den;

		break;

	case CE:
		if (output == 0)
			output = 1;

		loss_derivative = (((int32_t)label << (Q15_FRAC_BITS)) / output);
		break;
	}
	return (q15_t)loss_derivative;
}

q15_t q_node_activation_pd(q15_t node_output, activation_function_type act_func)
{
	int32_t return_value = 0;
	q15_t max_value = (1 << Q15_FRAC_BITS);

	switch (act_func)
	{
	case RELU:
		if (node_output > 0)
			return_value = max_value;
		else
			return_value = 0;
		break;

	case SIGMOID:
		return_value = (int32_t)node_output * (max_value - node_output);
		return_value = return_value >> 1;
		break;

	case TANH:
		return_value = ((int32_t)node_output * node_output);
		//return_value = return_value >> Q15_FRAC_BITS;
		return_value = return_value >> 1;
		return_value = max_value - return_value;
		break;

	default:
		break;
	}
	return return_value;
}

void q_hiddenLayer_delta(float* delta_inBuff, q7_t* weights, int8_t weights_fb, const unsigned char nextLayer_dim, q7_t* currLayer_out, activation_function_type act_func, const unsigned char currLayer_dim, float* delta_outBuff)
{
	float new_delta_value = 0;
	
	for (int node_idx = 0; node_idx < currLayer_dim; node_idx++)
	{
		for (int con_idx = 0; con_idx < nextLayer_dim; con_idx++)
		{
			float nl_delta = *(delta_inBuff + con_idx);

			q15_t q_nl_delta = (q15_t)round(nl_delta*256);
			q7_t q_weight = *(weights + con_idx * currLayer_dim + node_idx);
			int q_new_delta = q_nl_delta * q_weight;
			q_new_delta = q_new_delta >> weights_fb;		//mul shift
			new_delta_value += (float)q_new_delta/256.0;	//dequantize
		}
		q15_t q_output = *(currLayer_out + node_idx);
		q_output = q_output * 2;
			
		q15_t q_act = q_node_activation_pd(q_output, act_func);
		new_delta_value = new_delta_value * ((float)q_act/256.0);
	
		delta_outBuff[node_idx] = new_delta_value;
		new_delta_value = 0;
	}
}

void q_update_parameters(float* delta_inBuff, q7_t* layer_weights, uint8_t* weights_fb, q7_t* layer_bias, uint8_t* bias_fb, q7_t* prevLayer_out, const unsigned char prevLayer_dim, const unsigned char currLayer_dim, float learning_rate)
{
	// memory allocation to store updated parameters
	float* updated_weights = (float*)malloc(prevLayer_dim * currLayer_dim * sizeof(float));;
	float* updated_bias = (float*)malloc(currLayer_dim * sizeof(float));
	float previous_layer_out, weight, bias, learning_update;
	float abs_max_wt=0;
	float abs_max_bs=0;
	float abs_val = 0;
	
	q7_t q_layer_out;
	q7_t q_weight;
	q7_t q_bias;

	
	for (int node_idx = 0; node_idx < currLayer_dim; node_idx++) {

		float node_delta = *(delta_inBuff + node_idx);

		for (int out_idx = 0; out_idx < prevLayer_dim; out_idx++)
		{
			q_weight = *(layer_weights + prevLayer_dim * node_idx + out_idx);
			q_layer_out = *(prevLayer_out + out_idx);

			//	compute new weight
			previous_layer_out = (float)q_layer_out / 256.0;
			weight = (float)q_weight >>  (*weights_fb);

			learning_update = previous_layer_out * node_delta * learning_rate;
			float new_weight = weight - learning_update;
			*(updated_weights + prevLayer_dim * node_idx + out_idx) = new_weight;

			abs_val = fabs(new_weight);
			if(abs_val > abs_max_wt)
				abs_max_wt = abs_val;
		}

		//	compute new bias
		q_bias = *(layer_bias + node_idx);
		bias = (float)q_bias >> (*bias_fb);
		learning_update = learning_rate * node_delta;
		float new_bias = bias - learning_update;
		*(updated_bias + node_idx) = new_bias;

		abs_val = fabs(new_bias);
		if(abs_val > abs_max_bs)
			abs_max_bs = abs_val;
	}

	int wt_int_bits = (int)ceil(log2(abs_max_wt));
	if(wt_int_bits < 0)
		wt_int_bits = 0;
	else if(wt_int_bits > 7)
		wt_int_bits = 7;

	int bs_int_bits = (int)ceil(log2(abs_max_bs));
	if(bs_int_bits < 0)
		bs_int_bits = 0;
	else if(bs_int_bits > 7)
		bs_int_bits = 7;

	*weights_fb = 7-wt_int_bits;
	*bias_fb = 7-bs_int_bits;

	int conn_size = currLayer_dim * prevLayer_dim;
	q7_t* q_wt_ptr = layer_weights;
	q7_t* q_bs_ptr = layer_bias;
	for (int wt_idx = 0; wt_idx < conn_size; wt_idx++)
	{
		q7_t q_new_weight = 0;
		float new_weight = *(updated_weights + wt_idx);
		new_weight = round(new_weight >> (*weights_fb));
		q_new_weight = (q7_t)(__SSAT((int)new_weight, 8));
		*q_wt_ptr = q_new_weight;
		q_wt_ptr++;
	}
	
	for (int bs_idx = 0; bs_idx < currLayer_dim; bs_idx++)
	{
		q7_t q_new_bias = 0;
		float new_bias = *(updated_bias + bs_idx);
		new_bias = round(new_bias >> (*bias_fb));
		q_new_bias = (q7_t)(__SSAT((int)new_bias, 8));
		*q_bs_ptr = q_new_bias;
		q_bs_ptr++;
	}

	free(updated_weights);
	free(updated_bias);

}   

void q_back_prop(float* input_sample, float* out_trg, ann_model_q7* model_q7, float learning_rate)
{
	forward_propagation_buffers* fwd_p = (forward_propagation_buffers*)malloc(sizeof(forward_propagation_buffers));
	backward_propagation_buffers* bwd_p = (backward_propagation_buffers*)malloc(sizeof(backward_propagation_buffers));
	
	q_forward_propagation_buffers q_fwd_p;

	//quantized forward pass
	q7_t q_input_sample[6];
	uint8_t output_iw[3] = {3, 3, 3};
	uint8_t output_fb[3] = {4, 4, 4};
	uint8_t input_fb[3] = {7, 7, 7};

	q15_t buffer_aux[40];

	quantize_q7(input_sample, 6, 7, q_input_sample);
	forward_pass_q7(q_input_sample, model_q7, q_fwd_p.layer_0_out, q_fwd_p.layer_1_out, q_fwd_p.layer_2_out, buffer_aux, output_iw);
	
	q7_t* q_next_layer_wt;
	q7_t* q_next_layer_bs;
	q7_t* q_curr_layer_wt = model_q7->dense0_wt;
	q7_t* q_curr_layer_bs = model_q7->dense0_bias;
	q7_t* q_curr_layer_out = q_fwd_p.layer_0_out;
	q7_t* q_prev_layer_out;


	int index;

	//point to last layer
	for (index = 0; index < sizeof(model_q7->model_dimensions) - 2; index++)
	{	
		q_curr_layer_wt += model_q7->model_dimensions[index] * model_q7->model_dimensions[index + 1];
		q_curr_layer_bs += model_q7->model_dimensions[index + 1];
		q_curr_layer_out += model_q7->model_dimensions[index + 1];
	}

	q_prev_layer_out = q_curr_layer_out - model_q7->model_dimensions[index];

	//calculate layer delta values
	float* in_buff = bwd_p->delta1;
	float* out_buff = bwd_p->delta2;
	float* aux_ptr;

	q_outputLayer_delta(out_trg, q_curr_layer_out, model_q7->activation_functions[index], model_q7->model_dimensions[index + 1], out_buff);


	for (index = index - 1; index >= 0; index--) //for each layer
	{
		//swap delta buffers
		aux_ptr = in_buff;
		in_buff = out_buff;
		out_buff = aux_ptr;

		q_next_layer_wt = q_curr_layer_wt;
		q_next_layer_bs = q_curr_layer_bs;
		q_curr_layer_out = q_prev_layer_out;
		q_prev_layer_out = q_curr_layer_out - model_q7->model_dimensions[index];

		q_curr_layer_wt -= model_q7->model_dimensions[index] * model_q7->model_dimensions[index + 1];
		q_curr_layer_bs -= model_q7->model_dimensions[index + 1];


	q_hiddenLayer_delta(in_buff, q_next_layer_wt, model_q7->weights_fb[index+1], model_q7->model_dimensions[index + 2], q_curr_layer_out, model_q7->activation_functions[index], model_q7->model_dimensions[index + 1], out_buff);
	q_update_parameters(in_buff, q_next_layer_wt, &model_q7->weights_fb[index+1], q_next_layer_bs, &model_q7->bias_fb[index+1], q_curr_layer_out, model_q7->model_dimensions[index + 1], model_q7->model_dimensions[index + 2], learning_rate);
	}
	q_prev_layer_out = q_input_sample;
	q_update_parameters(out_buff, q_curr_layer_wt, &model_q7->weights_fb[0], q_curr_layer_bs, &model_q7->bias_fb[0], q_prev_layer_out, model_q7->model_dimensions[0], model_q7->model_dimensions[1], learning_rate);


	free(fwd_p);
	free(bwd_p);

	update_shifts(output_fb, input_fb, model_q7);
}

void q_outputLayer_delta(float* out_target, q7_t* layer_outs, activation_function_type act_func, const unsigned char layer_size, float* delta_outBuff)
{
	for (int i = 0; i < layer_size; i++)
	{
		float delta_value = 0;
		q7_t q_output_value = *(layer_outs + i);
		float output_target = *(out_target + i);

		q15_t q_label = (q15_t)round(output_target*256);
		q15_t q_model_out = (q15_t)q_output_value * 2;
		q15_t loss_pd = q_node_loss_pd(BCE, q_label, q_model_out);
		q15_t act_pd = q_node_activation_pd(q_model_out, act_func);

		delta_value = (loss_pd * act_pd) >> 8;		//quantized MUL shift
		delta_value = delta_value / 256;			//dequantize delta_value
																										
		delta_outBuff[i] = delta_value;
	}
}
